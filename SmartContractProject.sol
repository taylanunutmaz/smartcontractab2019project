pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

contract Project {
    
    struct musteri_bilgileri{
        bytes32 firma_hash;
        uint8 bakiye;
    }
    
    bytes32[] public musteri_hash; //müşletrilen ünvanlarının hashlenerek tutulduğu değişken
    mapping(bytes32=>musteri_bilgileri) bilgiler; //müşteri hashlerinden yararlanılarak bilgilerin çekildiği map yapısı
    
    //musteri bilgisi, firma bilgisi ve bakiye bilgisi ile map'e yeni müşteri ekleme
    function musteriEkle(string memory musteri, string memory firma, uint8 bakiye)
    public 
    returns(bytes32 musteriHash, bytes32 firmaHash, uint8 musteriBakiye) { 
        
        bytes32 key = keccak256(bytes(musteri));
        bilgiler[key].firma_hash = keccak256(bytes(firma));
        bilgiler[key].bakiye = bakiye;
        
        return(key, bilgiler[key].firma_hash, bilgiler[key].bakiye);
        
    }
    
    //daha önce eklenmiş bir müşteriye yeni bakiye transferi
    function bakiyeEkle(string memory musteri, uint8 eklenecekBakiye)
    public 
    returns(bytes32 musteriHash, bytes32 firmaHash, uint8 musteriBakiye){
        
        bytes32 key = keccak256(bytes(musteri));
        bilgiler[key].bakiye += eklenecekBakiye;
        
        return(key, bilgiler[key].firma_hash, bilgiler[key].bakiye);
        
    }
    
    //daha önce map'e kaydedilmiş müşterilerin harcamalarını işlenmesi
    function hacama(string memory musteri, uint8 harcananTutar)
    public 
    returns(bytes32 musteriHash, bytes32 firmaHash, uint8 Harcanan, uint8 SonrakiBakiye, string memory islemDurumu){
        
        bytes32 key = keccak256(bytes(musteri));
        
        if(bilgiler[key].bakiye > harcananTutar) {
            bilgiler[key].bakiye -= harcananTutar;
            return (key, bilgiler[key].firma_hash, harcananTutar, bilgiler[key].bakiye,"basarili");
        }
        else {
            return(key,bilgiler[key].firma_hash,0,bilgiler[key].bakiye,"bakiye yetersiz");
        }
        
    }
    
}